/*
 * Copyright © 2021 Advanced Micro Devices, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sub license, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NON-INFRINGEMENT. IN NO EVENT SHALL THE COPYRIGHT HOLDERS, AUTHORS
 * AND/OR ITS SUPPLIERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial portions
 * of the Software.
 */

/*
 * NIR viewer is a tool to view/compare logs obtained using NIR_PRINT=1
 * environment variable.
 *
 * It's based on dear imgui example application for SDL2 + OpenGL.
 */

#include "imgui.h"
#include "imgui_impl_opengl3.h"
#include "imgui_impl_sdl.h"
#include <GL/glew.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <algorithm>
#include <SDL.h>
#include <cstdlib>
#include <fcntl.h>
#include <fstream>
#include <iostream>
#include <map>
#include <mutex>
#include <pthread.h>
#include <regex.h>
#include <sstream>
#include <stdio.h>
#include <unistd.h>
#include <vector>

namespace Header
{
enum Type {
   None = -1,
   Shader = 0,
   Name,
   Inputs,
   InputsRead,
   Outputs,
   OutputsWritten,
   Uniforms,
   Shared,
   Ubos,
   SourceBlake3,
   RayQueries,
   SubgroupSize,
   BitSizesInt,
   BitSizesFloat,
   SystemValuesRead,
   FlrpLowered,
   IOLowered,
   NeedsQuadHelperInvocation,
   Internal,
   Stage,
   NextStage,
   NumTextures,
   TexturesUsed,
   SamplersUsed,
   FirstUBOIsDefaultUBO,
   DivergenceAnalysisRun,
   NumUbos,
   TexturesUsedByTxf,
   InlinableUniformDwOffsets,
   NumInlinableUniforms,
   UsesDiscard,
   NumImages,
   ImagesUsed,
   WritesMemory,
   Count
};
}

struct header {
   const char *start, *end;
};

namespace Theme
{
enum Enum {
   Background,
   Text,
   TextImportant,
   Keyword,
   Intrinsic,
   Type,
   VariableName,
   Comment,
   Ssa,
   Number,
   Block,
   Highlight,
   Tabs,
   LineNumberImportant
};
}

class ColorTheme {
   public:
      virtual ~ColorTheme() {}
      virtual const char * hexstr(Theme::Enum e) = 0;
      virtual ImColor imcolor(Theme::Enum e) = 0;
};

// Colors from https://ethanschoonover.com/solarized/
namespace Solarized {
   const char *_hexstr[] = {
      "#002b36", // base03
      "#073642", // base02
      "#586e75", // base01
      "#657b83", // base00
      "#839496", // base0
      "#93a1a1", // base1
      "#eee8d5", // base2
      "#fdf6e3", // base3
      "#b58900", // yellow
      "#cb4b16", // orange
      "#dc322f", // red
      "#d33682", // magenta
      "#6c71c4", // violet
      "#268bd2", // blue
      "#2aa198", // cyan
      "#859900", // green
   };
   // Colors from https://ethanschoonover.com/solarized/
   const int _rgb[][3] = {
      { 0, 43, 54 },
      { 7, 54, 66 },
      { 88, 110, 117 },
      { 101, 123, 131 },
      { 131, 148, 150 },
      { 147, 161, 161 },
      { 238, 232, 213 },
      { 253, 246, 227 },
      { 181, 137, 0 },
      { 203, 75, 22 },
      { 220, 50, 47 },
      { 211, 54, 130 },
      { 108, 113, 196 },
      { 38, 139, 210 },
      { 42, 161, 152 },
      { 133, 153, 0 },
   };
   enum Enum {
      base03 = 0,
      base02,
      base01,
      base00,
      base0,
      base1,
      base2,
      base3,
      yellow,
      orange,
      red,
      magenta,
      violet,
      blue,
      cyan,
      green,
   };

   int DarkThemeMapping[] = {
      Solarized::base03,
      Solarized::base00,
      Solarized::base1,
      Solarized::yellow,
      Solarized::blue,
      Solarized::orange,
      Solarized::magenta,
      Solarized::base01,
      Solarized::cyan,
      Solarized::green,
      Solarized::magenta,
      Solarized::base3,
      Solarized::base02,
      Solarized::red,
   };
   int LightThemeMapping[] = {
      Solarized::base3,
      Solarized::base0,
      Solarized::base01,
      Solarized::yellow,
      Solarized::blue,
      Solarized::orange,
      Solarized::magenta,
      Solarized::base1,
      Solarized::cyan,
      Solarized::green,
      Solarized::magenta,
      Solarized::base03,
      Solarized::base2,
      Solarized::red,
   };

   class Theme : public ColorTheme {
      public:
         Theme(bool dark) {
            mapping = dark ? DarkThemeMapping : LightThemeMapping;
         }
         const char * hexstr(::Theme::Enum e) {
            return _hexstr[mapping[e]];
         }
         ImColor imcolor(::Theme::Enum e) {
            const int *c = _rgb[mapping[e]];
            return ImColor(c[0], c[1], c[2]);
         }
      private:
         int *mapping;
   };
}

class ImguiTheme : public ColorTheme {
   public:
      const char * hexstr(::Theme::Enum e) {
         static char c[32];
         ImColor color = this->imcolor(e);
         sprintf(c, "#%02x%02x%02x", (int)(color.Value.x * 255), (int)(color.Value.y * 255), (int)(color.Value.z * 255));
         return c;
      }
      ImColor imcolor(::Theme::Enum e) {
         switch (e) {
            case Theme::Background: return ImColor(ImGui::GetStyleColorVec4(ImGuiCol_WindowBg));
            case Theme::Text: return ImColor(ImGui::GetStyleColorVec4(ImGuiCol_Text));
            case Theme::TextImportant: return ImColor(ImGui::GetStyleColorVec4(ImGuiCol_Text));
            case Theme::Keyword: return ImColor(ImGui::GetStyleColorVec4(ImGuiCol_Text));
            case Theme::Type: return ImColor(ImGui::GetStyleColorVec4(ImGuiCol_Text));
            case Theme::Intrinsic:
               return ImColor(ImGui::GetStyleColorVec4(ImGuiCol_Text));
            case Theme::VariableName: return ImColor(ImGui::GetStyleColorVec4(ImGuiCol_Text));
            case Theme::Comment: return ImColor(ImGui::GetStyleColorVec4(ImGuiCol_TextDisabled));
            case Theme::Ssa: return ImColor(ImGui::GetStyleColorVec4(ImGuiCol_Text));
            case Theme::Number: return ImColor(ImGui::GetStyleColorVec4(ImGuiCol_Text));
            case Theme::Block: return ImColor(ImGui::GetStyleColorVec4(ImGuiCol_Text));
            case Theme::Highlight: return ImColor(ImGui::GetStyleColorVec4(ImGuiCol_TabActive));
            case Theme::Tabs: return ImColor(ImGui::GetStyleColorVec4(ImGuiCol_Tab));
            case Theme::LineNumberImportant:return ImColor(ImGui::GetStyleColorVec4(ImGuiCol_Text));
         }
         return ImColor();
      }
};

ColorTheme *theme = NULL;

Header::Type lookup_header_type(char *line, char *line_end, struct header *hdr)
{
   char *colon = strchr(line, (int)':');
   if (!colon || colon > line_end)
      return Header::None;

   hdr->start = colon + 1 + 1;
   hdr->end = line_end - 1;

   if (strncmp(line, "shader:", strlen("shader:")) == 0)
      return Header::Shader;
   if (strncmp(line, "name:", strlen("name:")) == 0)
      return Header::Name;
   if (strncmp(line, "inputs:", strlen("inputs:")) == 0)
      return Header::Inputs;
   if (strncmp(line, "outputs:", strlen("outputs:")) == 0)
      return Header::Outputs;
   if (strncmp(line, "uniforms:", strlen("uniforms:")) == 0)
      return Header::Uniforms;
   if (strncmp(line, "shared:", strlen("shared:")) == 0)
      return Header::Shared;
   if (strncmp(line, "source_blake3:", strlen("source_blake3:")) == 0)
      return Header::SourceBlake3;
   if (strncmp(line, "ray queries:", strlen("ray queries:")) == 0)
      return Header::RayQueries;
   if (strncmp(line, "ubos:", strlen("ubos:")) == 0)
      return Header::Ubos;
   if (strncmp(line, "num_ubos:", strlen("num_ubos:")) == 0)
      return Header::NumUbos;
   if (strncmp(line, "inputs_read:", strlen("inputs_read:")) == 0)
      return Header::InputsRead;
   if (strncmp(line, "outputs_written:", strlen("outputs_written:")) == 0)
      return Header::OutputsWritten;
   if (strncmp(line, "system_values_read:", strlen("system_values_read:")) == 0)
      return Header::SystemValuesRead;
   if (strncmp(line, "subgroup_size:", strlen("subgroup_size:")) == 0)
      return Header::SubgroupSize;
   if (strncmp(line, "bit_sizes_int:", strlen("bit_sizes_int:")) == 0)
      return Header::BitSizesInt;
   if (strncmp(line, "bit_sizes_float:", strlen("bit_sizes_float:")) == 0)
      return Header::BitSizesFloat;
   if (strncmp(line, "flrp_lowered:", strlen("flrp_lowered:")) == 0)
      return Header::FlrpLowered;
   if (strncmp(line, "io_lowered:", strlen("io_lowered:")) == 0)
      return Header::IOLowered;
   if (strncmp(line, "needs_quad_helper_invocations:", strlen("needs_quad_helper_invocations:")) == 0)
      return Header::NeedsQuadHelperInvocation;
   if (strncmp(line, "internal:", strlen("internal:")) == 0)
      return Header::Internal;
   if (strncmp(line, "stage:", strlen("stage:")) == 0)
      return Header::Stage;
   if (strncmp(line, "next_stage:", strlen("next_stage:")) == 0)
      return Header::NextStage;
   if (strncmp(line, "num_textures:", strlen("num_textures:")) == 0)
      return Header::NumTextures;
   if (strncmp(line, "textures_used:", strlen("textures_used:")) == 0)
      return Header::TexturesUsed;
   if (strncmp(line, "textures_used_by_txf:", strlen("textures_used_by_txf:")) == 0)
      return Header::TexturesUsedByTxf;
   if (strncmp(line, "samplers_used:", strlen("samplers_used:")) == 0)
      return Header::SamplersUsed;
   if (strncmp(line, "first_ubo_is_default_ubo:", strlen("first_ubo_is_default_ubo:")) == 0)
      return Header::FirstUBOIsDefaultUBO;
   if (strncmp(line, "divergence_analysis_run:", strlen("divergence_analysis_run:")) == 0)
      return Header::DivergenceAnalysisRun;
   if (strncmp(line, "inlinable_uniform_dw_offsets:", strlen("inlinable_uniform_dw_offsets:")) == 0)
      return Header::InlinableUniformDwOffsets;
   if (strncmp(line, "num_inlinable_uniforms:", strlen("num_inlinable_uniforms:")) == 0)
      return Header::NumInlinableUniforms;
   if (strncmp(line, "uses_discard:", strlen("uses_discard:")) == 0)
      return Header::UsesDiscard;
   if (strncmp(line, "num_images:", strlen("num_images:")) == 0)
      return Header::NumImages;
   if (strncmp(line, "images_used:", strlen("images_used:")) == 0)
      return Header::ImagesUsed;
   if (strncmp(line, "writes_memory:", strlen("writes_memory:")) == 0)
      return Header::WritesMemory;

   return Header::None;
}

struct code_blocks {
   int id;
   std::vector<int> successors;
   unsigned line_start, line_end;
   bool folded;
   bool highlighted;
   int depth;
};
bool code_block_cmp(code_blocks &cb1, code_blocks &cb2) {
   return cb1.line_start < cb2.line_start;
}


struct nir_pass {
   struct {
      char *start, *end;
   } title;
   struct {
      char *start, *end;
   } code;

   struct header headers[Header::Count];
   std::vector<unsigned> line_offsets;
   std::vector<code_blocks> blocks;
   std::map<int, code_blocks*> id_to_block;
   std::map<int, ImVec4> frames;
   ImVec4 cfg_graph_bbox;

   std::vector<unsigned> selected;
};

struct nir_shader {
   std::vector<struct nir_pass> passes;
   char *name;
   char *sha1;
};

const char *shader_names[] = {
   "MESA_SHADER_VERTEX",
   "MESA_SHADER_TESS_CTRL",
   "MESA_SHADER_TESS_EVAL",
   "MESA_SHADER_GEOMETRY",
   "MESA_SHADER_FRAGMENT",
   "MESA_SHADER_COMPUTE",
   "MESA_SHADER_TASK",
   "MESA_SHADER_MESH",
};
#define SHADER_TYPES ((int)(sizeof(shader_names)/sizeof(char*)))
static int sh_prefix_offset = strlen("MESA_SHADER_");

static bool fuzzy_match_simple(char *pattern, const char *str, const char *str_end) {
  while (*pattern != '\0' && str != str_end)  {
      if (tolower(*pattern) == tolower(*str))
          ++pattern;
      ++str;
  }
  return *pattern == '\0' ? true : false;
}

const char *valid_pass_start[] = {
   "nir_", "gl_nir_", "st_nir_", "si_nir_", "ac_nir_",
   "si_", "glsl_to_nir",
   "lower_bindless", "match_tex_dests"};

bool is_pass_start(char *ptr)
{
   for (int i = 0; i < IM_ARRAYSIZE(valid_pass_start); i++) {
      if (strncmp(ptr, valid_pass_start[i], strlen(valid_pass_start[i])) == 0)
         return true;
   }
   return false;
}

char *
search_next_pass_start(char *ptr, int len)
{
   char *next = NULL, *p;

   for (int i = 0; i < IM_ARRAYSIZE(valid_pass_start);) {
      /* Don't search further than the current result. */
      if (next)
         len = next - ptr;

      p = (char *)memmem(ptr, len, valid_pass_start[i], strlen(valid_pass_start[i]));

      if (i == 0) {
         if (p >= ptr + 3) {
            /* Is it gl_nir? */
            if (strncmp(p - 3, "gl_", 3) == 0)
               p = p - 3;
            /* Or maybe st_nir? */
            else if (strncmp(p - 3, "st_", 3) == 0)
               p = p - 3;
            /* Or maybe si_nir? */
            else if (strncmp(p - 3, "si_", 3) == 0)
               p = p - 3;
            /* Or maybe ac_nir? */
            else if (strncmp(p - 3, "ac_", 3) == 0)
               p = p - 3;
         }
         /* skip other prefixes with nir_ */
         i = 5;
      } else {
         i++;
      }

      if (p) {
         assert(!next || p <= next);
         next = p;
      }
   }

   if (!next) {
      next = strstr(ptr, "shader: MESA_");
   }
   return next;
}

static void add_arrow(ImVec2 p1, ImVec2 p2, ImU32 col)
{
   float dx = p2.x - p1.x;
   float dy = p2.y - p1.y;
   float l = sqrt(dx * dx + dy * dy);
   ImVec2 ta = ImVec2(dx / l, dy / l);
   ImVec2 no(-ta.y, ta.x);
   ImVec2 center((p1.x + p2.x) * 0.5, (p1.y + p2.y) * 0.5);
   int s = 6;
   ImVec2 ba(center.x - s * ta.x, center.y - s * ta.y);
   ImVec2 q(ba.x + no.x * s, ba.y + no.y * s);
   ImVec2 r(ba.x - no.x * s, ba.y - no.y * s);
   ImGui::GetWindowDrawList()->AddTriangleFilled(center, q, r, col);
}

struct file_content {
   file_content()
   {
      pthread_mutex_init(&this->mtx, NULL);
   }

   pthread_mutex_t mtx;
   std::vector<struct nir_shader> shaders[SHADER_TYPES];
   int src_fd;
   const char *file_mmaped;
};
std::vector<struct file_content *> files;

struct loading_job {
   char *filename;
   struct file_content *out;
   float *progress;
};

static void force_redraw() {
   SDL_Event evt;
   evt.type = SDL_USEREVENT;
   SDL_PushEvent(&evt);
}

const char *dummy_pass = "N/A";

void *read_nir_file(void *_job)
{
   struct loading_job *job = (struct loading_job *)_job;
   char *filename = job->filename;
   struct file_content &out = *job->out;

   *job->progress = 0;

   struct stat stats;
   int fd = open(filename, 0);

   if (!fd) {
      printf("Coulnd't open file : '%s'\n", filename);
      return NULL;
   }

   if (fstat(fd, &stats)) {
      printf("Error reading '%s'\n", filename);
      close(fd);
      return NULL;
   }
   char *data = (char *)mmap(NULL, stats.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
   if (data == MAP_FAILED) {
      close(fd);
      printf("Coulnd't mmap file : '%s' (size: %ld)\n", filename, stats.st_size);
      return NULL;
   }
   out.file_mmaped = data;
   out.src_fd = fd;

   for (uint64_t i = search_next_pass_start(data, stats.st_size) - data; i < (uint64_t)stats.st_size;) {
      struct nir_pass current_pass = {0};
      char *ptr = NULL;

      if (is_pass_start(&data[i])) {
         /* The first line is the title of the pass */
         current_pass.title.start = &data[i];
         current_pass.title.end = strchr(&data[i], (int)'\n');
         ptr = current_pass.title.end + 1;
      } else {
         current_pass.title.start = (char*)dummy_pass;
         current_pass.title.end = current_pass.title.start + 3;
         ptr = &data[i];
      }

      /* Read headers ("shader: xxx", "uniforms: xxx", ...) */
      struct header hdr;
      do {
         char *next_line_end = strchr(ptr, (int)'\n');
         enum Header::Type type = lookup_header_type(ptr, next_line_end, &hdr);

         if (type == Header::None)
            break;

         current_pass.headers[type] = hdr;
         ptr = next_line_end + 1;
      } while (true);

      if (current_pass.headers[Header::Shader].start == NULL) {
         /* Empty header => no-op pass */
         i = search_next_pass_start(ptr, data + stats.st_size - ptr) - data;
         continue;
      }

      /* Now idenfify where the code begins/ends */
      current_pass.code.start = ptr;
      current_pass.code.end = search_next_pass_start(ptr, data + stats.st_size - ptr);
      if (!current_pass.code.end)
         current_pass.code.end = data + stats.st_size;
      current_pass.line_offsets.push_back(0);
      {
         char *s = current_pass.code.start;
         /* Search line beginnings */
         unsigned l = (unsigned)(current_pass.code.end - current_pass.code.start) + 1;
         while ((s = (char *)memchr(s, '\n', l))) {
            s++;
            current_pass.line_offsets.push_back((unsigned)(s - current_pass.code.start));
            l = (unsigned)(current_pass.code.end - s);
         }
         current_pass.line_offsets.push_back(
            (unsigned)(current_pass.code.end - current_pass.code.start) + 1);
      }

      /* Move the read pointer directly to the end of this pass */
      i = current_pass.code.end - data;

      /* Store this pass in the shader. We need to use a lock since the UI reads from
       * this structure too */
      pthread_mutex_lock(&out.mtx);
      bool added = false;
      for (int j = 0; j < SHADER_TYPES; j++) {
         /* Find oud the correct shader type */
         if (strncmp(&current_pass.headers[Header::Shader].start[sh_prefix_offset],
                     &shader_names[j][sh_prefix_offset],
                     strlen(shader_names[j]) - sh_prefix_offset) == 0) {
            int name_len = 0;
            if (current_pass.headers[Header::Name].start)
               name_len = (int)(current_pass.headers[Header::Name].end -
                                current_pass.headers[Header::Name].start) +
                          1;
            int sha1_len = (int)(current_pass.headers[Header::SourceBlake3].end -
                                 current_pass.headers[Header::SourceBlake3].start) +
                           1;

            struct nir_shader *dst = NULL;
            /* No shaders? Create a new one */
            if (out.shaders[j].empty()) {
               struct nir_shader n;
               n.name = name_len ? strndup(current_pass.headers[Header::Name].start, name_len) : NULL;
               n.sha1 = sha1_len > 1 ? strndup(current_pass.headers[Header::SourceBlake3].start, sha1_len) : strdup("xxxxxx");
               out.shaders[j].push_back(n);
               dst = &out.shaders[j].back();
            } else {
               for (auto it = out.shaders[j].rbegin(); it != out.shaders[j].rend(); ++it) {
                  bool same;
                  /* If we already have a name, we can simply compare with the new name. */
                  if (it->name && name_len) {
                     same = strncmp(current_pass.headers[Header::Name].start, it->name, name_len) == 0;
                  } else {
                     /* Otherwise compare the hash. */
                     same = strncmp(current_pass.headers[Header::SourceBlake3].start, it->sha1, sha1_len) == 0;
                  }
                  if (same) {
                     dst = &(*it);

                     /* Update the name if needed. */
                     if (dst->name == NULL && name_len) {
                        dst->name = strndup(current_pass.headers[Header::Name].start, name_len);
                     }
                     break;
                  }
               }

               /* Create a new shader if the SourceSha1 is different */
               if (!dst) {
                  struct nir_shader n;
                  n.name = name_len ? strndup(current_pass.headers[Header::Name].start, name_len) : NULL;
                  n.sha1 = strndup(current_pass.headers[Header::SourceBlake3].start, sha1_len);
                  /* sort shaders alphabetically */
                  for (auto it = out.shaders[j].begin(); it != out.shaders[j].end(); ++it) {
                     if (!it->name || strncmp(current_pass.headers[Header::Name].start, it->name, name_len) < 0) {
                        /* insert here */
                        dst = &(*out.shaders[j].insert(it, n));
                        break;
                     }
                  }

                  if (!dst) {
                     out.shaders[j].push_back(n);
                     dst = &out.shaders[j].back();
                  }
               }
            }
            dst->passes.push_back(current_pass);
            added = true;
            break;
         }
      }
      assert(added);
      *job->progress = (i / (float)stats.st_size);
      force_redraw();
      pthread_mutex_unlock(&out.mtx);
   }

   printf("File: %s\n", filename);
   for (int j = 0; j < SHADER_TYPES; j++) {
      printf("%lu shaders %s\n", out.shaders[j].size(), shader_names[j]);
      for (size_t k = 0; k < out.shaders[j].size(); k++) {
         printf("\t- %s: %lu passes\n", out.shaders[j][k].name, out.shaders[j][k].passes.size());
      }
   }

   force_redraw();

   return 0;
}

int is_nir_line_in_pass(nir_pass &p, const char *line, int size)
{
   /* Skip whitespaces */
   int skip = 0;
   while (line[0] == '\t' || line[0] == ' ' || line[0] == '\n') {
      line++;
      size--;
      skip++;
   }

   int max_needle_offset = -1;

   /* compare everything except ssa_XXX */
   for (int i = 0; i < (int) p.line_offsets.size() - 1; i++) {
      bool precise_mode = false;
      char *haystack = &p.code.start[p.line_offsets[i]];
      char *endline = &p.code.start[p.line_offsets[i + 1] - 1];

      int offset_haystack = 0;
      int offset_needle = 0;
      int len = endline - haystack + 1;
      do {
         int needle_size;
         char *ssa = NULL;

         if (precise_mode) {
            /* We failed already, but try to get a more precise cursor */
            const char *next_space = (const char*) memchr(&line[offset_needle], ' ', size - offset_needle);
            needle_size = (next_space ? (next_space - line + 1):size) - offset_needle;
            if (needle_size == 0)
               break;
         } else {
            /* Search the next ssa_ */
            ssa = (char *) memmem(&line[offset_needle], size - offset_needle,
                                        "ssa_", 4);

            if (ssa) {
               needle_size = ssa - line - offset_needle;
            } else {
               needle_size = size - offset_needle;
            }
         }

         /* Compare everything up to this point */
         char *r = (char*) memmem(&haystack[offset_haystack], len - offset_haystack,
                                  &line[offset_needle], needle_size);
         if (r) {
            offset_haystack = r - haystack;
            offset_needle += needle_size;

            if (ssa) {
               offset_needle += 4;
               while (line[offset_needle] >= '0' && line[offset_needle] <= '9')
                  offset_needle++;
            }

            max_needle_offset = std::max(max_needle_offset, offset_needle);
         } else {
            if (!precise_mode)
               precise_mode = true;
            else
               break;
         }
         if (offset_needle == size) {
            return 0;
         }
      } while(true);
   }
   return max_needle_offset > 0 ? max_needle_offset + skip : -1;
}

struct syntax_coloring {
   const char *expression;
   regex_t preg;
   size_t nmatch;
   Theme::Enum colors[7];
};

std::vector<syntax_coloring> regexps = {
   {/* Variables declaration */
    .expression = "(decl_var)\\s([a-z_]*)?\\s([A-Z_]+)\\s([a-z]*\\s)*([a-zA-Z0-9_]*)(\\[[0-9]*\\])?\\s([a-zA-Z0-9_]*)?",
    .colors = {Theme::Keyword, Theme::Type, Theme::Comment, Theme::Text, Theme::Type, Theme::Type, Theme::VariableName}},
   {
      /* NIR intrinsic */
      .expression = "(\\s)?(@[a-z0-9_]+)\\s(&[a-zA-Z0-9_]*)?",
      .colors = {Theme::Text, Theme::Intrinsic, Theme::VariableName},
   },
   {
      /* NIR opcode */
      .expression = "(\\s)?([a-z0-9_]+)\\s(&[a-zA-Z0-9_]*)?",
      .colors = {Theme::Text, Theme::Keyword, Theme::VariableName},
   },
   {
      .expression = "\\b(loop|else|if)",
      .colors = {Theme::Keyword},
   },
   {
      /* ssa variable */
      .expression = "(%[[:digit:]]+)",
      .colors = {Theme::Ssa},
   },
   {
      /* parameters */
      .expression = "([a-zA-Z0-9_]+)=([a-zA-Z0-9_]+)",
      .colors = {Theme::Text, Theme::Number},
   },
   {
      /* comment */
      .expression = "(//)(.*)",
      .colors = {Theme::Comment, Theme::Comment},
   },
   {
      /* types */
      .expression = "\\b(vec[[:digit:]]|mat[[:digit:]]|float[[:digit:]]?[[:digit:]]?|bool|sampler[123]D|int|uint\\b)",
      .colors = {Theme::Type},
   },
   {
      /* number */
      .expression = "(0x[a-z0-9]*)",
      .colors = {Theme::Number},
   },
   {
      /* blocks */
      .expression = "(block\\s)(b[0-9]*)",
      .colors = {Theme::Block, Theme::Block},
   },
   {
      /* convergence */
      .expression = "\\b(con)\\b",
      .colors = {Theme::Highlight},
   },
   {
      /* divergence */
      .expression = "\\b(div)\\b",
      .colors = {Theme::Comment},
   },
};

static regmatch_t *pmatch;

static void prepare_colored_line(struct syntax_coloring *reg, const char *in_line, int size, char *line, int write_cursor)
{
   assert(reg == NULL || pmatch[0].rm_so >= 0);

   int start = 0, end = 0;
   if (reg) {
      for (size_t group = 1; group <= reg->nmatch; group++) {
         start = pmatch[group].rm_so;

         /* Optional group */
         if (start < 0)
            continue;

         /* Copy everything until the match */
         memcpy(&line[write_cursor], &in_line[end], start - end);
         write_cursor += start - end;

         end = pmatch[group].rm_eo;
         /* Insert the color code, and copy the matching part */
         write_cursor += sprintf(&line[write_cursor], "%s%.*s", theme->hexstr(reg->colors[group - 1]),
                                                                    end - start,
                                                                    &in_line[start]);
      }
   }

   /* Copy the reminder */
   memcpy(&line[write_cursor], &in_line[end], size - end);
   write_cursor += size - end;
   char copy[4096];
   line[write_cursor] = '\0';

   /* Apply generic coloring: ssa values, NIR types, ... */
   for (size_t i = 4; i < regexps.size(); i++) {
      char *ptr = copy;
      write_cursor = 0;
      strcpy(copy, line);
      size = strlen(copy);
      start = 0;
      end = 0;
      bool one_match = false;

      while (true) {
         if ((regexec(&regexps[i].preg, ptr, regexps[i].nmatch + 1, pmatch, 0) == REG_NOMATCH) ||
             (pmatch[0].rm_so == -1))
            break;

         for (size_t group = 1; group <= regexps[i].nmatch; group++) {
            start = pmatch[group].rm_so;
            if (start < 0)
               continue;

            one_match = true;

            /* Copy everything until the match */
            memcpy(&line[write_cursor], &ptr[end], start - end);
            write_cursor += start - end;

            end = pmatch[group].rm_eo;
            /* Insert the color code, and copy the matching part */
            write_cursor += sprintf(&line[write_cursor], "%s%.*s%s", theme->hexstr(regexps[i].colors[group - 1]),
                                                                   end - start,
                                                                   &ptr[start],
                                                                   theme->hexstr(Theme::Text));
         }
         ptr += end;
         end = 0;
      }

      if (one_match && size > 0) {
         /* Copy the reminder */
         memcpy(&line[write_cursor], ptr, size - (ptr - copy) + 1);
         write_cursor += size - (ptr - copy) + 1;
         line[write_cursor] = '\0';
      }
   }
}

void draw_line(char *line) {
   static char clicked_element[256] = {0};
   int clicked_element_len = strlen(clicked_element);

   ImGui::SameLine(0, 0);

   while (line && *line) {
      ImGui::SameLine(0, 0);

      char *next_ssa = strstr(line, theme->hexstr(Theme::Ssa));

      char *pattern_start;
      if (next_ssa && (pattern_start = strchr(next_ssa, '%')))  {
         char *pattern_end = strchr(pattern_start, '#');
         int pattern_len = pattern_end - pattern_start;

         ImGui::TextUnformatted(line, next_ssa);
         ImGui::SameLine(0, 0);

         ImVec2 min = ImGui::GetCursorScreenPos();
         ImVec2 size = ImGui::CalcTextSize(next_ssa, pattern_end);
         ImVec2 max = min;
         max.x += size.x;
         max.y += size.y;

         /* If this ssa value is hovered or selected, draw a quad over it. */
         if (ImGui::IsMouseHoveringRect(min, max) ||
             (pattern_len == clicked_element_len && strncmp(pattern_start, clicked_element, clicked_element_len) == 0)) {

            min.x -= 2;
            max.x += 2;
            min.y -= 2;
            max.y += 2;
            ImGui::GetWindowDrawList()->AddRectFilled(min, max, theme->imcolor(Theme::Highlight));
            ImGui::GetWindowDrawList()->AddRect(min, max, theme->imcolor(Theme::Ssa));
         }
         /* Draw the ssa text. */
         ImGui::TextUnformatted(next_ssa, pattern_end);

         if (ImGui::IsItemClicked()) {
            if (pattern_len == clicked_element_len && strncmp(pattern_start, clicked_element, clicked_element_len) == 0) {
               clicked_element[0] = '\0';
            } else {
               strncpy(clicked_element, pattern_start, pattern_len);
               clicked_element[pattern_len] = '\0';
            }
         }
         line = pattern_end;
      } else {
         ImGui::TextUnformatted(line);
         break;
      }
   }
}

static int print_decl_var(const char * const in_line, int size, char *line, int write_cursor) {
   void *decl_var = memmem(in_line, size, "decl_var", strlen("decl_var"));
   if (!decl_var)
      return 0;

   if ((regexec(&regexps[0].preg, in_line, regexps[0].nmatch + 1, pmatch, 0) == REG_NOMATCH) ||
       (pmatch[0].rm_so == -1))
      return 0;

   prepare_colored_line(&regexps[0], in_line, size, line, write_cursor);
   draw_line(line);
   return 1;
}

static int print_instruction(const char * const in_line, int size, char *line, int write_cursor) {
   char *cf = (char *)memmem(in_line, size, "= ", strlen("= "));
   if (cf) {
      cf += strlen("= ");
      size -= strlen("= ");
   } else {
      cf = (char *)in_line;
   }

   char *comment = (char *)memmem(cf, size, "//", strlen("//"));
   if (comment)
      size -= cf + size - comment;

   int idx = -1;
   int regs[] = {1 /* intrinsic */, 2 /* opcode */};
   for (int i = 0; i < IM_ARRAYSIZE(regs); i++) {
      int r = regexec(&regexps[regs[i]].preg, cf, regexps[regs[i]].nmatch + 1, pmatch, 0);
      if (r == REG_NOMATCH || (pmatch[0].rm_so == -1))
         continue;
      idx = regs[i];
      break;
   }
   if (idx < 0)
      return 0;

   memcpy(&line[write_cursor], in_line, cf - in_line);
   write_cursor += cf - in_line;
   prepare_colored_line(&regexps[idx], cf, size, line, write_cursor);

   draw_line(line);

   return 1;
}

static int print_cf(const char * const in_line, int size, char *line, int write_cursor, code_blocks *next_block) {
   const char *cfs[] = { "else" , "loop ", "if" };
   for (int i = 0; i < 3; i++) {
      void *cf = memmem(in_line, size, cfs[i], strlen(cfs[i]));
      if (!cf)
         continue;

      if ((regexec(&regexps[3].preg, in_line, regexps[3].nmatch + 1, pmatch, 0) == REG_NOMATCH) ||
          (pmatch[0].rm_so == -1))
         return 0;

      prepare_colored_line(&regexps[3], in_line, size, line, write_cursor);
      ImGui::SameLine(0, 0);
      bool folded = !ImGui::TreeNodeEx(&line[3], ImGuiTreeNodeFlags_NoTreePushOnOpen | ImGuiTreeNodeFlags_NoAutoOpenOnLog | ImGuiTreeNodeFlags_DefaultOpen);
      if (next_block && next_block->folded != folded) {
         next_block->folded = folded;
         force_redraw();
      }

      return 1;
   }
   return 0;
}

static bool display_line(const char * const in_line, char *line, int write_cursor, int size, code_blocks *current_blk, code_blocks *next_blk, float *start_x)
{
   static bool regexp_ready = false;

   if (!regexp_ready) {
      size_t max_nmatch = 0;
      for (size_t i = 0; i < regexps.size(); i++) {
         if (regcomp(&regexps[i].preg, regexps[i].expression, REG_EXTENDED) != 0) {
            printf("REGEXP '%s' failed\n", regexps[i].expression);
            continue;
         }

         regexps[i].nmatch = regexps[i].preg.re_nsub;
         assert (regexps[i].nmatch > 0);
         max_nmatch = std::max(max_nmatch, regexps[i].nmatch);
      }

      pmatch = (regmatch_t *)malloc(sizeof(regmatch_t) * (max_nmatch + 1));

      regexp_ready = true;
   }

   int read_cursor = 0;

   /* Indentation markers */
   unsigned indent = 0;
   while (in_line[read_cursor] == '\t') {
      write_cursor += sprintf(&line[write_cursor], "   ");
      read_cursor++;
      indent++;
   }

   *start_x = ImGui::GetCursorScreenPos().x + indent * ImGui::CalcTextSize("   ").x;

   ImVec2 line_start = ImGui::GetCursorScreenPos();

   int r = print_decl_var(&in_line[read_cursor], size - read_cursor, line, write_cursor);
   if (r == 0) {
      r = print_cf(&in_line[read_cursor], size - read_cursor, line, write_cursor, next_blk);
   }
   if (r == 0) {
      r = print_instruction(&in_line[read_cursor], size - read_cursor, line, write_cursor);
   }

   if (r <= 0) {
      prepare_colored_line(NULL, &in_line[read_cursor], size - read_cursor, line, write_cursor);
      draw_line(line);
   }

   ImGui::SameLine();
   ImVec2 line_end = ImGui::GetCursorScreenPos();
   line_start.y -= ImGui::GetTextLineHeight() * 0.2;
   line_end.y += ImGui::GetTextLineHeight() * 1.2;
   ImGui::NewLine();

   bool line_is_hovered = ImGui::IsMouseHoveringRect(line_start, line_end);
   if (current_blk && line_is_hovered)
      current_blk->highlighted = true;

   return line_is_hovered;
}

void parse_blocks(nir_pass& pass)
{
   /* Parse blocks from this pass */
   size_t num_lines = pass.line_offsets.size() - 1;
   std::vector<code_blocks> blocks;
   for (size_t l = 0; l < num_lines; l++) {
      char *start =
         &pass.code.start[pass.line_offsets[l]];
      char *endline =
         &pass.code.start[pass.line_offsets[l + 1] - 1];

      const char *block_start = (const char *)memmem(start, endline - start, "block b", strlen("block b"));
      if (block_start) {
         char *line = (char *)alloca(endline - block_start + 1);
         memcpy(line, block_start, endline - block_start);
         line[endline - block_start] = '\0';

         int id;
         if (sscanf(line, "block b%d:", &id) == 1) {
            code_blocks cb;
            cb.line_start = l;
            cb.line_end = l;
            cb.id = id;
            cb.folded = false;
            cb.depth = pass.blocks.empty() ? 0 : pass.blocks.back().depth + 1;
            blocks.push_back(cb);
         } else {
            assert(false);
         }
      }

      const char *preds = (const char *)memmem(start, endline - start, "// preds: ", strlen("// preds: "));
      if (preds) {
         /* Parse predecessors */
         char *line = (char *)alloca(endline - preds + 1);
         memcpy(line, preds, endline - preds);
         line[endline - preds] = '\0';
         char *s;
         int predecessor_min_depth = -1;
         while ((s = strstr(line, "b"))) {
            int myid = -1;
            s += strlen("b");
            if (sscanf(s, "%d", &myid) == 1) {
               for (auto blk: pass.blocks) {
                  if (blk.id == myid) {
                     predecessor_min_depth = predecessor_min_depth < 0 ? blk.depth : std::max(predecessor_min_depth, blk.depth);
                     break;
                  }
               }
               line = s;
               blocks[blocks.size() - 1].depth = predecessor_min_depth + 1;
            } else {
               assert(false);
               break;
            }
         }
      }
      const char *succs = (const char *)memmem(start, endline - start, "// succs: ", strlen("// succs: "));
      if (succs == NULL)
         succs = (const char *)memmem(start, endline - start, ", succs: ", strlen(", succs: "));
      if (succs) {
         code_blocks &cb = blocks[blocks.size() - 1];
         cb.line_end = l - 1;
         /* Parse successors */
         char *line = (char *)alloca(endline - succs + 1);
         memcpy(line, succs, endline - succs);
         line[endline - succs] = '\0';
         char *s;
         while ((s = strstr(line, "b"))) {
            int myid = -1;
            s += strlen("b");
            if (sscanf(s, "%d", &myid) == 1) {
               cb.successors.push_back(myid);
               line = s;
            } else {
               assert(false);
               break;
            }
         }
         pass.blocks.push_back(cb);
         blocks.pop_back();
      }
   }
   pass.blocks.push_back(blocks.back());
   std::sort(pass.blocks.begin(), pass.blocks.end(), code_block_cmp);
   for (auto& blk: pass.blocks) {
      pass.id_to_block[blk.id] = &blk;
   }
}

void prepare_control_flow_graph(nir_pass& pass)
{
   std::vector<std::pair<int, int>> to_draw;
   std::map<int, int> depth_offset;
   to_draw.push_back(std::make_pair(0, 0));
   ImVec4 bbox(FLT_MAX, FLT_MAX, 0, 0);

   while (!to_draw.empty()) {
      int active_blk = to_draw.back().second;
      int previous_level = to_draw.back().first;
      to_draw.pop_back();

      const auto& blk = *pass.id_to_block[active_blk];
      if (depth_offset.find(blk.depth) == depth_offset.end()) {
         depth_offset[blk.depth] = 0;
      }

      /* Figure out the deepest successor's level offset */
      int deepest = blk.depth;
      for (int s: blk.successors) {
         deepest = std::max(deepest, pass.id_to_block[s]->depth);
      }

      int offset = depth_offset[blk.depth];
      for (auto it: depth_offset) {
         if (blk.depth < it.first && it.first <= deepest)
            offset = std::max(offset, it.second);
      }

      for (int i = previous_level + 1; i < blk.depth; i++) {
         depth_offset[i] = std::max(depth_offset[i], offset + 1);
      }

      ImVec2 base(-20, 0);
      base.x -= offset * 80;
      depth_offset[blk.depth] = offset + 1;
      base.y += blk.depth * 80;

      ImVec2 tl = base;
      tl.x -= 60;
      ImVec2 br = base;
      br.y += 60;

      pass.frames[blk.id] = ImVec4(tl.x, tl.y, br.x, br.y);

      bbox.x = std::min(bbox.x, tl.x - 20);
      bbox.y = std::min(bbox.y, tl.y);
      bbox.z = std::max(bbox.z, br.x + 20);
      bbox.w = std::max(bbox.w, br.y);

      std::vector<int> nexts;
      for (int s: blk.successors) {
         if (pass.frames.find(s) == pass.frames.end())
            nexts.push_back(s);
      }
      std::sort(nexts.begin(), nexts.end(), [pass] (int a, int b) {
         const auto &c1 = pass.id_to_block.find(a)->second;
         const auto &c2 = pass.id_to_block.find(b)->second;
         return c1->depth < c2->depth;
      });
      for (auto b: nexts)
         to_draw.push_back(std::make_pair(blk.depth, b));
   }

   pass.cfg_graph_bbox = bbox;
}

int draw_control_flow_graph(const nir_pass& pass, float frame_width)
{
   const auto& blocks = pass.blocks;
   const auto& id_to_block = pass.id_to_block;
   /* Copy frames because we're modifing them */
   auto frames = pass.frames;

   int hovered = -1;
   int scroll_to_line = -1;

   /* Shift frames to account for the current window position + handle mouse interactions */
   for (auto& it: frames) {
      it.second.x += ImGui::GetWindowPos().x + frame_width;
      it.second.z += ImGui::GetWindowPos().x + frame_width;
      it.second.y += ImGui::GetWindowPos().y - ImGui::GetScrollY();
      it.second.w += ImGui::GetWindowPos().y - ImGui::GetScrollY();

      code_blocks *b = id_to_block.find(it.first)->second;

      if (b->highlighted)
         hovered = it.first;
      if (ImGui::IsMouseHoveringRect(ImVec2(it.second.x, it.second.y), ImVec2(it.second.z, it.second.w))) {
         hovered = it.first;
         if (ImGui::IsMouseClicked(0)) {
            scroll_to_line = b->line_start;
         }
      }
   }

   /* Draw arrows connectings the blocks */
   for (const auto &blk: blocks) {
      ImVec4 s = frames[blk.id];
      ImVec2 p1((s.x + s.z) * 0.5, s.w);
      for (int succ: blk.successors) {
         ImVec4 e = frames[succ];
         ImVec2 p2;
         bool going_down = false;
         bool same_col = false;
         if (e.y > s.w) {
            going_down = true;
            p2 = ImVec2((e.x + e.z) * 0.5, e.y);
         } else {
            if (std::abs(s.x - e.x) < 10) {
               same_col = true;
               p1 = ImVec2(s.z, (s.y + s.w) * 0.5);
               p2 = ImVec2(e.z, (e.y + e.w) * 0.5);
            } else if (s.x < e.x) {
               p1 = ImVec2(s.z, (s.y + s.w) * 0.5);
               p2 = ImVec2(e.x, (e.y + e.w) * 0.5);
            } else {
               p1 = ImVec2(s.x, (s.y + s.w) * 0.5);
               p2 = ImVec2(e.z, (e.y + e.w) * 0.5);
            }
         }

         ImU32 col = (hovered == blk.id || hovered == succ) ? (ImU32)theme->imcolor(Theme::Highlight) : IM_COL32_BLACK;
         if (std::abs(p2.y - p1.y) < 30) {
            ImGui::GetWindowDrawList()->AddLine(p1, p2, col);
            add_arrow(p1, p2, col);
         } else if (!going_down && same_col) {
            /* Add intermediate points */
            ImVec2 p3 = p1;
            p3.x += 10;
            p3.y -= 10;
            ImGui::GetWindowDrawList()->AddLine(p1, p3, col);
            ImVec2 p4 = p2;
            p4.x += 10;
            p4.y += 10;
            ImGui::GetWindowDrawList()->AddLine(p3, p4, col);
            ImGui::GetWindowDrawList()->AddLine(p4, p2, col);
            add_arrow(p1, p3, col);
            add_arrow(p3, p4, col);
            add_arrow(p4, p2, col);
         } else {
            /* Add an intermediate point */
            ImVec2 p3 = p2;
            p2.x = p1.x * 0.9 + p3.x * 0.1;
            p2.y = p1.y * 0.1 + p3.y * 0.9;
            ImGui::GetWindowDrawList()->AddLine(p1, p2, col);
            ImGui::GetWindowDrawList()->AddLine(p2, p3, col);
            add_arrow(p1, p2, col);
            add_arrow(p2, p3, col);
         }
      }
   }

   /* Draw the blocks */
   for (auto it: frames) {
      char txt[128];
      sprintf(txt, "blk %d", it.first);
      ImVec2 size = ImGui::CalcTextSize(txt);
      ImVec2 tl = ImVec2(it.second.x, it.second.y);
      ImVec2 br = ImVec2(it.second.z, it.second.w);
      ImVec2 tx((tl.x + br.x - size.x) * 0.5, (tl.y + br.y - size.y) * 0.5);
      ImGui::GetWindowDrawList()->AddRectFilled(tl, br, theme->imcolor(Theme::TextImportant), 4);
      ImGui::GetWindowDrawList()->AddRect(tl, br, hovered == it.first ? (ImU32)theme->imcolor(Theme::Block) : IM_COL32_BLACK, 4);
      ImGui::GetWindowDrawList()->AddText(tx, IM_COL32_BLACK, txt);
   }

   return scroll_to_line;
}

int main(int argc, char **argv)
{
   if (argc < 2) {
      printf("Usage: nir_viewer filename [filename] ...\n");
      exit(1);
   }

   std::vector<float> percent_loaded;
   files.resize(argc - 1);
   percent_loaded.resize(argc - 1);
   /* Start a thread for each file */
   for (int i = 1; i < argc; i++) {
      percent_loaded[i - 1] = 0;
      struct file_content *f = new file_content();
      files[i - 1] = f;

      struct loading_job *job = new loading_job();
      job->filename = argv[i];
      job->out = f;
      job->progress = &percent_loaded[i - 1];
      pthread_t t_id;
      pthread_create(&t_id, NULL, read_nir_file, job);
   }

   if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_GAMECONTROLLER) != 0) {
      printf("Error: %s\n", SDL_GetError());
      return -1;
   }

   // GL 3.0 + GLSL 130
   const char *glsl_version = "#version 130";
   SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, 0);
   SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
   SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
   SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);

   // Create window with graphics context
   SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
   SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
   SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
   SDL_WindowFlags window_flags =
      (SDL_WindowFlags)(SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI);
   SDL_Window *window = SDL_CreateWindow("NIR viewer", SDL_WINDOWPOS_CENTERED,
                                         SDL_WINDOWPOS_CENTERED, 1280, 720, window_flags);
   SDL_GLContext gl_context = SDL_GL_CreateContext(window);
   SDL_GL_MakeCurrent(window, gl_context);
   SDL_GL_SetSwapInterval(1); // Enable vsync

   // Initialize OpenGL loader
   GLenum err = glewInit();
   if (err != GLEW_OK && err != GLEW_ERROR_NO_GLX_DISPLAY) {
      fprintf(stderr, "Failed to initialize OpenGL loader!\n");
      return 1;
   }

   // Setup Dear ImGui context
   IMGUI_CHECKVERSION();
   ImGui::CreateContext();
   ImGuiIO &io = ImGui::GetIO();
   (void)io;

   ImGui::StyleColorsDark();

   int theme_index = 1;
   ColorTheme *themes[] = {
      new ImguiTheme(),
      new Solarized::Theme(true),
      new Solarized::Theme(false)
   };
   const char *theme_names[] = {
      "Default", "Solarized Dark", "Solarized Light"
   };
   theme = themes[theme_index];


   ImGui::PushStyleColor(ImGuiCol_WindowBg, theme->imcolor(Theme::Background).Value);
   ImGui::PushStyleColor(ImGuiCol_Text, theme->imcolor(Theme::TextImportant).Value);
   ImGui::PushStyleColor(ImGuiCol_Tab, theme->imcolor(Theme::Background).Value);
   ImGui::PushStyleColor(ImGuiCol_TabActive, theme->imcolor(Theme::Tabs).Value);
   ImGui::PushStyleColor(ImGuiCol_HeaderHovered, theme->imcolor(Theme::Tabs).Value);

   // Setup Platform/Renderer bindings
   ImGui_ImplSDL2_InitForOpenGL(window, gl_context);
   ImGui_ImplOpenGL3_Init(glsl_version);

   // Our state
   ImVec4 clear_color = ImColor(0, 43, 54, 255).Value;

   float yScroll = -1;

   std::vector<nir_pass *> pass_to_compare;
   for (int i = 1; i < argc; i++)
      pass_to_compare.push_back(NULL);

   char pass_to_highlight[128] = {0};
   char nir_to_highlight[128] = {0};
   int pass_with_diagram[128] = {0};
   struct {
      char filename[256];
      int result;
      float timer;
   } save_shader = {0};


   int scroll_to_line = -1;

   struct timespec before;
   clock_gettime(CLOCK_MONOTONIC, &before);

   // Main loop
   bool done = false;
   bool only_show_shaders_with_pass = true;
   while (!done) {
      struct timespec now;
      clock_gettime(CLOCK_MONOTONIC, &now);
      float dt = now.tv_sec - before.tv_sec;
      if (now.tv_nsec < before.tv_nsec) {
         dt += ((1000000000 + now.tv_nsec) - before.tv_nsec) * 0.000000001 - 1;
      } else {
         dt += (now.tv_nsec - before.tv_nsec) * 0.000000001;
      }
      memcpy(&before, &now, sizeof(now));

   event_handling:
      SDL_Event event;
      if (SDL_WaitEvent(&event)) {
         ImGui_ImplSDL2_ProcessEvent(&event);
         if (event.type == SDL_QUIT)
            done = true;
         if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE &&
             event.window.windowID == SDL_GetWindowID(window))
            done = true;
      }

      if (SDL_PeepEvents(&event, 1, SDL_PEEKEVENT, SDL_FIRSTEVENT, SDL_LASTEVENT) > 0)
         goto event_handling;

      // Start the Dear ImGui frame
      ImGui_ImplOpenGL3_NewFrame();
      ImGui_ImplSDL2_NewFrame(window);
      ImGui::NewFrame();

      // Create a window called "My First Tool", with a menu bar.
      ImGui::SetNextWindowPos(ImVec2(0, 0));
      int w, h;
      SDL_GetWindowSize(window, &w, &h);
      ImGui::SetNextWindowSize(ImVec2(w, h));
      ImGui::Begin("NIR viewer", NULL,
                   ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoScrollbar |
                   ImGuiWindowFlags_NoScrollWithMouse | ImGuiWindowFlags_NoResize);

      ImVec2 avail = ImGui::GetContentRegionAvail();
      avail.x -= 2 * ImGui::GetStyle().WindowPadding.x;

      ImGui::Columns(3);
      ImGui::Text("Search pass by name:");
      ImGui::SameLine();
      ImGui::PushID("pass name");
      ImGui::InputText("", pass_to_highlight, sizeof(pass_to_highlight));
      ImGui::PopID();
      ImGui::Checkbox("Only show matching shaders", &only_show_shaders_with_pass);
      ImGui::NextColumn();
      ImGui::Text("Highlight:");
      ImGui::SameLine();
      ImGui::PushID("nir code");
      ImGui::InputText("", nir_to_highlight, sizeof(nir_to_highlight));
      ImGui::PopID();
      ImGui::NextColumn();
      if (ImGui::Combo("Theme", &theme_index, theme_names, 3)) {
         theme = themes[theme_index];
         ImGui::PopStyleColor(5);
         ImGui::PushStyleColor(ImGuiCol_WindowBg, theme->imcolor(Theme::Background).Value);
         ImGui::PushStyleColor(ImGuiCol_Text, theme->imcolor(Theme::TextImportant).Value);
         ImGui::PushStyleColor(ImGuiCol_Tab, theme->imcolor(Theme::Background).Value);
         ImGui::PushStyleColor(ImGuiCol_TabActive, theme->imcolor(Theme::Tabs).Value);
         ImGui::PushStyleColor(ImGuiCol_HeaderHovered, theme->imcolor(Theme::Tabs).Value);
         force_redraw();
      }
      ImGui::Columns(1);
      ImGui::NewLine();

      for (int arg = 1; arg < argc; arg++) {
         int file_idx = arg - 1;
         int next_file_idx = (file_idx + 1) % files.size();
         file_content *current_file = files[file_idx];
         file_content *next_file = files[next_file_idx];

         char wid[128];
         sprintf(wid, "win%d", arg);
         pthread_mutex_lock(&current_file->mtx);
         ImGui::SameLine();
         ImGui::BeginChild(wid, ImVec2(avail.x / (argc - 1), 0), false,
                           ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoScrollbar |
                              ImGuiWindowFlags_NoScrollWithMouse);
         char progr[512];
         int total_shader_count = 0;
         for (int j = 0; j < SHADER_TYPES; j++) {
            total_shader_count += current_file->shaders[j].size();
         }
         sprintf(progr, "%s (%d shaders)", argv[arg], total_shader_count);
         ImGui::PushStyleColor(ImGuiCol_Text, IM_COL32_WHITE);
         ImGui::ProgressBar(percent_loaded[file_idx], ImVec2(avail.x / (argc - 1), 0.0), progr);
         ImGui::PopStyleColor();

         char tabbarid[128];
         sprintf(tabbarid, "tabbar%d", arg);
         if (ImGui::BeginTabBar(tabbarid, ImGuiTabBarFlags_None)) {
            for (int j = 0; j < SHADER_TYPES; j++) {
               char tmp[128];

               if (current_file->shaders[j].empty())
                  continue;

               if (percent_loaded[file_idx] >= 1)
                  sprintf(tmp, "%s [%ld]", &shader_names[j][5], current_file->shaders[j].size());
               else
                  sprintf(tmp, "%s [...]", &shader_names[j][5]);
               if (!ImGui::BeginTabItem(tmp)) {
                  continue;
               }

               if (!ImGui::BeginTabBar(shader_names[j], ImGuiTabBarFlags_FittingPolicyScroll |
                                                           ImGuiTabBarFlags_TabListPopupButton)) {
                  continue;
               }
               for (size_t k = 0; k < current_file->shaders[j].size(); k++) {
                  struct nir_shader &sh = current_file->shaders[j][k];
                  char name[256];
                  char line[4096];
                  char temp[4096];

                  bool shader_has_pass = false;
                  if (strlen(pass_to_highlight) >= 3) {
                     /* Highlight shaders with the pass */
                     for (size_t p = 0; p < sh.passes.size(); p++) {
                        if (fuzzy_match_simple(pass_to_highlight, sh.passes[p].title.start, sh.passes[p].title.end)) {
                           shader_has_pass = true;
                           break;
                        }
                     }
                     if (!shader_has_pass && only_show_shaders_with_pass)
                        continue;
                  }
                  sprintf(name, "%s%s", (!only_show_shaders_with_pass && shader_has_pass) ? theme->hexstr(Theme::LineNumberImportant) : "", sh.name);
                  ImGui::PushID(&sh);

                  bool tab_is_open = ImGui::BeginTabItem(name);

                  if (ImGui::IsItemHovered()) {
                     ImGui::BeginTooltip();
                     ImGui::Text("Blake3: %s", sh.sha1);
                     ImGui::EndTooltip();
                  }

                  if (!tab_is_open) {
                     ImGui::PopID();
                     continue;
                  }

                  float top_most_y = ImGui::GetCursorScreenPos().y;
                  ImGui::BeginChild(name);
                  ImGui::Text("Blake3: %s", sh.sha1);
                  ImGui::BeginDisabled(strlen(save_shader.filename) == 0);
                  if (ImGui::Button("Save to File")) {
                     int fd = open(save_shader.filename, O_RDWR | O_TRUNC | O_CREAT, S_IRWXU);
                     if (fd >= 0) {
                        for (auto &p : sh.passes) {
                           off64_t start = p.title.start - current_file->file_mmaped;
                           off64_t end = p.code.end - current_file->file_mmaped;
                           size_t len = end - start;

                           copy_file_range(
                              current_file->src_fd, &start,
                              fd, NULL,
                              len, 0);
                        }
                        close(fd);
                        save_shader.result = 0;
                     } else {
                        save_shader.result = fd;
                     }
                     save_shader.timer = 3;
                  }
                  ImGui::EndDisabled();
                  ImGui::SameLine();
                  ImGui::InputTextWithHint("", "enter filename", save_shader.filename, sizeof(save_shader.filename));
                  if (save_shader.timer > 0) {
                     ImGui::SameLine();
                     if (save_shader.result < 0) {
                        ImGui::Text("%sFailed (%s)",
                                    theme->hexstr(Theme::TextImportant),
                                    strerror(-save_shader.result));
                     } else {
                        ImGui::Text("%sDone",
                                    theme->hexstr(Theme::Comment));
                     }
                     save_shader.timer -= dt;
                     force_redraw();
                  }

                  for (size_t p = 0; p < sh.passes.size(); p++) {
                     const char *highlight = "";
                     if (pass_to_highlight[0] != '\0' &&
                         fuzzy_match_simple(pass_to_highlight, sh.passes[p].title.start, sh.passes[p].title.end)) {
                        highlight = theme->hexstr(Theme::LineNumberImportant);
                     }

                     char label[512];
                     {
                        /* delta compared to next file */
                        int off = 0;
                        int delta =
                           argc > 2
                              ? 0
                              : (sh.passes[p].line_offsets.size() - 1) -
                                   (next_file->shaders[j][k].passes[p].line_offsets.size() - 1);
                        off = sprintf(label, "%s%.*s (#657b83%lu #657b83lines",
                                      highlight,
                                      (int)(sh.passes[p].title.end - sh.passes[p].title.start),
                                      sh.passes[p].title.start,
                                      sh.passes[p].line_offsets.size() - 1);
                        if (delta != 0) {
                           sprintf(&label[off], " %s%d)", delta >= 0 ? "#859900+" : "#dc322f-",
                                   std::abs(delta));
                        } else {
                           sprintf(&label[off], ")");
                        }
                     }

                     if (ImGui::TreeNodeEx((void *)&sh.passes[p], 0, "%s", label)) {
                        float force_scroll = -1;
                        bool compare_source = (pass_to_compare[file_idx] == &sh.passes[p]);
                        bool diagram = (pass_with_diagram[arg] - 1) == (int)p;

                        if (ImGui::Checkbox("Compare", &compare_source)) {
                           pass_to_compare[file_idx] = compare_source ? &sh.passes[p] : NULL;
                        }
                        ImGui::SameLine();
                        if (ImGui::Checkbox("Control-flow", &diagram)) {
                           pass_with_diagram[arg] = diagram ? (p + 1) : 0;
                        }
                        ImGui::SameLine();
                        if (ImGui::Button("Copy To Clipboard")) {
                           int size = 4096;
                           char *clip = (char *)realloc(NULL, size);
                           int n = 0;
                           for (unsigned sl = 0; sl < sh.passes[p].line_offsets.size() - 1; sl++) {
                              char *start =
                                 &sh.passes[p].code.start[sh.passes[p].line_offsets[sl]];
                              char *endline =
                                 &sh.passes[p].code.start[sh.passes[p].line_offsets[sl + 1] - 1];

                              int len = endline - start;
                              if (n + len > size) {
                                 size *= 2;
                                 clip = (char *)realloc(clip, size);
                              }

                              snprintf(&clip[n], size - n, "%.*s\n", len, start);
                              n += len + 1;
                           }
                           ImGui::SetClipboardText(clip);
                        }
                        ImGui::SameLine();
                        if (ImGui::Button("Copy Selection to Clipboard")) {
                           char clip[4096];
                           int n = 0;
                           for (auto sl : sh.passes[p].selected) {
                              char *start =
                                 &sh.passes[p].code.start[sh.passes[p].line_offsets[sl]];
                              char *endline =
                                 &sh.passes[p].code.start[sh.passes[p].line_offsets[sl + 1] - 1];

                              int len = endline - start;
                              snprintf(&clip[n], sizeof(clip) - n, "%.*s\n", len, start);
                              n += len + 1;
                           }
                           ImGui::SetClipboardText(clip);
                        }

                        if (sh.passes[p].blocks.empty())
                           parse_blocks(sh.passes[p]);
                        if (diagram && sh.passes[p].frames.empty())
                           prepare_control_flow_graph(sh.passes[p]);

                        ImVec2 passZoneBegin = ImGui::GetCursorScreenPos();
                        passZoneBegin.x = ImGui::GetWindowPos().x;
                        ImGuiListClipper clipper;
                        clipper.Begin(sh.passes[p].line_offsets.size() - 1);
                        float line_start_x = ImGui::GetCursorScreenPos().x;
                        ImVec2 current_block_start;

                        int right_align_line_n = 0;
                        int num_lines = sh.passes[p].line_offsets.size() - 1;
                        while (num_lines) {
                           num_lines /= 10;
                           right_align_line_n++;
                        }

                        for (auto &b : sh.passes[p].blocks)
                           b.highlighted = false;

                        while (clipper.Step()) {
                           int current_block_index = -1;
                           int line_offset = 0;
                           for (unsigned l = 0; l < sh.passes[p].line_offsets.size() - 1; l++) {
                              for (size_t b = current_block_index + 1; b < sh.passes[p].blocks.size(); b++) {
                                 if (l == sh.passes[p].blocks[b].line_start) {
                                    /* Entering a new block */
                                    current_block_index = b;
                                    current_block_start.y = ImGui::GetCursorScreenPos().y;
                                    break;
                                 }
                              }
                              if (scroll_to_line < 0 && clipper.DisplayEnd <= (int)l - line_offset)
                                 break;

                              code_blocks *current = NULL;
                              if (current_block_index >= 0)
                                 current = &sh.passes[p].blocks[current_block_index];

                              /* Check if this line is clipped */
                              if (scroll_to_line >= 0 || clipper.DisplayStart <= (int)l - line_offset) {
                                 char *start =
                                    &sh.passes[p].code.start[sh.passes[p].line_offsets[l]];
                                 char *endline =
                                    &sh.passes[p].code.start[sh.passes[p].line_offsets[l + 1] - 1];

                                 ImVec2 cursor = ImGui::GetCursorPos();
                                 cursor.y += +ImGui::GetTextLineHeightWithSpacing() * 0.5;

                                 if (scroll_to_line >= 0) {
                                    if (scroll_to_line == (int)l) {
                                       force_scroll = ImGui::GetCursorPos().y;
                                       scroll_to_line = -1;
                                    } else {
                                       ImGui::NewLine();
                                       continue;
                                    }
                                 }

                                 /* New NIR line */
                                 int size = (int)(endline - start);
                                 strncpy(temp, start, size);
                                 temp[size] = 0;

                                 bool highlight = false;
                                 auto selected = std::find(sh.passes[p].selected.begin(), sh.passes[p].selected.end(), l);
                                 if (selected != sh.passes[p].selected.end()) {
                                    highlight = true;
                                 } else if (*nir_to_highlight != '\0' && fuzzy_match_simple(nir_to_highlight, temp, &temp[size])) {
                                    highlight = true;
                                 }
                                 if (highlight) {
                                    ImVec2 min = ImGui::GetCursorScreenPos();
                                    ImVec2 max = min;
                                    max.x += ImGui::GetWindowWidth();
                                    float h = ImGui::GetTextLineHeight();
                                    max.y += h * 1.2;
                                    min.y -= h * 0.2;
                                    ImGui::GetWindowDrawList()->AddRectFilled(min, max, theme->imcolor(Theme::Tabs));
                                 }
                                 int wr = 0, rd = 0;
                                 wr += sprintf(&line[wr], "%s%*d ",
                                               theme->hexstr(highlight ? Theme::LineNumberImportant : Theme::Comment),
                                               right_align_line_n, l + 1);

                                 ImGui::TextUnformatted(line, &line[wr]);
                                 ImGui::SameLine();
                                 wr = 0;

                                 /* In single file mode, diff is against the previous/next passes */
                                 if (argc == 2) {
                                    int match = 0;
                                    /* Check if the line is new in this pass */
                                    if (p > 0 && size &&
                                        (match = is_nir_line_in_pass(sh.passes[p - 1], temp, size))) {
                                       strcpy(&line[wr], "#859900+");
                                       wr += strlen("#00ff00+");
                                    } else {
                                       line[wr] = ' ';
                                       wr++;
                                    }
                                    /* Check if the line is deleted in next pass */
                                    if (p < (sh.passes.size() - 1) && size &&
                                        (match = is_nir_line_in_pass(sh.passes[p + 1], temp, size))) {
                                       strcpy(&line[wr], "#dc322f-");
                                       wr += strlen("#00ff00-");
                                    } else {
                                       line[wr] = ' ';
                                       wr++;
                                    }
                                    line[wr++] = ' ';
                                 }
                                 /* In multifile mode, diff is against the other pass */
                                 else {
                                    for (int i = 0; i < 3; i++)
                                       line[wr++] = ' ';

                                    if (pass_to_compare[next_file_idx]) {
                                       int match;
                                       char *needle = temp;
                                       int s = size;
                                       if (s && compare_source &&
                                           (match = is_nir_line_in_pass(*pass_to_compare[next_file_idx], needle, s))) {

                                          strcpy(&line[wr - 3], "#dc322f<> ");
                                          wr += strlen("#dc322f<> ") - 3;
                                          ImVec2 ppp(line_start_x, ImGui::GetCursorScreenPos().y);
                                          ImVec2 ppp2(line_start_x, ppp.y + ImGui::GetTextLineHeightWithSpacing());

                                          float f1 = ImGui::GetCursorScreenPos().x + ImGui::CalcTextSize(needle, &needle[match + 1]).x;
                                          float f2 = f1 + ImGui::CalcTextSize(&needle[match + 1]).x;
                                          ppp.x = f1;
                                          ppp.y = ppp2.y;
                                          ppp2.x = f2;
                                          ImGui::GetWindowDrawList()->AddLine(ppp, ppp2, ImColor(0xf5, 0x19, 0.5), 1);
                                       }
                                    }
                                 }

                                 code_blocks *next = NULL;
                                 if (current_block_index < (int)sh.passes[p].blocks.size() - 1)
                                    next = &sh.passes[p].blocks[current_block_index + 1];

                                 ImGui::PushID(l);
                                 bool hovered = display_line(&temp[rd], line, wr, size, current, next, &current_block_start.x);
                                 ImGui::PopID();

                                 if (hovered && ImGui::IsMouseClicked(0)) {
                                    if (selected == sh.passes[p].selected.end())
                                       sh.passes[p].selected.push_back(l);
                                    else
                                       sh.passes[p].selected.erase(selected);
                                 }
                              }

                              if (current_block_index >= 0 && l <= current->line_end) {
                                 ImVec2 end = ImVec2(current_block_start.x, ImGui::GetCursorScreenPos().y);
                                 ImGui::GetWindowDrawList()->AddLine(current_block_start, end, current->highlighted ? theme->imcolor(Theme::Block) : theme->imcolor(Theme::Keyword), 1);
                              }

                              if (current_block_index >= 0 && current->folded && current->line_start == l) {
                                 int skip = current->line_end - (current->line_start + 1) + 1;
                                 l += skip;
                                 line_offset += skip;
                              }
                           }
                        }
                        clipper.End();

                        if (force_scroll > 0) {
                           ImGui::SetScrollY(force_scroll);
                           force_redraw();
                           force_scroll = -1;
                        }

                        if (ImGui::IsMouseDoubleClicked(0)) {
                           ImVec2 passZoneEnd = ImGui::GetCursorScreenPos();
                           passZoneEnd.x = passZoneBegin.x + ImGui::GetWindowWidth();
                           if (ImGui::IsMouseHoveringRect(passZoneBegin, passZoneEnd)) {
                              pass_to_compare[file_idx] = compare_source ? NULL : &sh.passes[p];
                           }
                        }

                        if (diagram) {
                           const ImVec4 &bbox = sh.passes[p].cfg_graph_bbox;
                           float frame_width = (bbox.z - bbox.x);
                           ImGui::SetNextWindowPos(ImVec2((arg)*avail.x / (argc - 1) - frame_width,
                                                          top_most_y + ImGui::GetStyle().WindowPadding.y));
                           ImGui::SetNextWindowSize(ImVec2(frame_width, avail.y - top_most_y - ImGui::GetStyle().WindowPadding.y));
                           ImGui::SetNextWindowContentSize(ImVec2(frame_width, bbox.w - bbox.y));
                           ImGui::SetNextWindowBgAlpha(0.8);
                           ImGui::Begin("graph", NULL, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoBackground);
                           scroll_to_line = draw_control_flow_graph(sh.passes[p], frame_width);
                           ImGui::End();
                        }

                        ImGui::TreePop();
                     }
                  }
                  if (ImGui::IsMouseClicked(1)) {
                     if (ImGui::IsWindowHovered()) {
                        yScroll = ImGui::GetScrollY();
                        SDL_Event evt;
                        evt.type = SDL_USEREVENT;
                        SDL_PushEvent(&evt);
                     }
                  } else if (yScroll >= 0) {
                     ImGui::SetScrollY(yScroll);
                     if (arg == argc - 1) {
                        yScroll = -1;
                     }
                  }

                  ImGui::EndChild();
                  ImGui::EndTabItem();
                  ImGui::PopID();
               }

               ImGui::EndTabBar();
               ImGui::EndTabItem();
            }

            ImGui::EndTabBar();
         }
         ImGui::EndChild();

         pthread_mutex_unlock(&current_file->mtx);
      }

      ImGui::End();

      // Rendering
      ImGui::Render();
      glViewport(0, 0, (int)io.DisplaySize.x, (int)io.DisplaySize.y);
      glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
      glClear(GL_COLOR_BUFFER_BIT);
      ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
      SDL_GL_SwapWindow(window);
   }

   // Cleanup
   ImGui_ImplOpenGL3_Shutdown();
   ImGui_ImplSDL2_Shutdown();
   ImGui::DestroyContext();

   SDL_GL_DeleteContext(gl_context);
   SDL_DestroyWindow(window);
   SDL_Quit();

   return 0;
}
